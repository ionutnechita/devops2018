require 'couchbase'
require 'jenkins_api_client'
require 'ostruct'
require 'open3'
require 'rocketchat'

module Devops


    @timestamp=%x{ date +"%Y-%m-%d-%H%M%S" }.delete("\n")
    @user_script=%x{ echo $USER }.delete("\n")
    @ch_1='"1 c [{"'
    @ch_2='"$ c }]"'
    @format='list'
    @path_cbimport_couchbase='/opt/couchbase/bin/cbimport'
    @dir_workspace='/DevOps/FileRelease'
    @path_url_kernel_release_json='https://www.kernel.org/releases.json'
    @working_dir = "/usr/local/src"

    def self.welcome
        puts '=========================================='
        puts 'Welcome ' + @user_script
        puts '=========================================='
        puts 'Timestamp is: ' + @timestamp
        puts '=========================================='
    end

    def self.exit_code
        if $?.exitstatus == 0
        then
            puts "OK. Command executed correctly." + "#{$?.exitstatus}"
        else
            puts "FAIL. Command executed not correctly." + "#{$?.exitstatus}"
            puts '=========================================='
            exit $?.exitstatus
        end
    end

    def self.strip(val)
        val[:rows]
    end
    
    def self.check_query(val)
        res = val[:meta]['status']
        
        if res != 'success'
        then
            puts "[ERROR] Problem on query."
            puts '=========================================='
            exit 1
        else
            puts "[OK] Query executed correctly."
        end
    end
    
    module Config
    	extend self
        attr_accessor :user
        attr_accessor :password
        attr_accessor :cluster

        def reset
            self.user = nil
            self.password = nil
            self.cluster = nil
        end

        reset
    end

    class << self
        def configure
		    block_given? ? yield(Config) : Config
        end

    	def config
            Config
        end
    end

    Devops.configure do |config|
        config.user = ENV['DB_User']
        config.password = ENV['DB_Password']
        config.cluster = ENV['DB_Cluster']
        raise 'Missing ENV[DB_User]!' unless config.user
        raise 'Missing ENV[DB_Password]!' unless config.password
        raise 'Missing ENV[DB_Cluster]!' unless config.cluster
    end

    @channel = ENV['ROCKETCHAT_CHANNEL']
    raise 'Missing ENV[ROCKETCHAT_CHANNEL]!' unless @channel

    def self.db(bucket)
        Couchbase.connect( :bucket => "#{bucket}", :host => "#{config.cluster}", :username => "#{config.user}", :password => "#{config.password}" )
    end

    def self.db_query(query, bucket)
        puts "[INFO] Start query: #{query}"
        val = db(bucket).query(query)
        check_query(val)
        return val
    end

    def self.jenkins(bucket, shortname_jenkins)
        begin
            puts '=========================================='
            puts '[INFO] Start connection on jenkins. '
            res = db_query("SELECT hostname_jenkins, username_jenkins, password_jenkins from `#{bucket}` where meta().id=':#{shortname_jenkins}'", bucket)

            counter_entry = strip(res).first
            if counter_entry.nil?
            then
                    puts "[INFO] Maybe is wrong. Not exist entry. Rerun it."
                    exit 1
            else
                    hostname_jenkins = strip(res).first['hostname_jenkins']
                    username_jenkins = strip(res).first['username_jenkins']
                    password_jenkins = strip(res).first['password_jenkins']
            end
    
            client = JenkinsApi::Client.new(:server_ip => hostname_jenkins,
                     :username => username_jenkins, :password => password_jenkins, :log_level => 'WARN')
            puts client.get_server_date
            puts '=========================================='
            return client
        rescue Net::OpenTimeout => @e
            abort "ERROR #{@e}"
        end
    end

    def self.jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
        conn = jenkins(bucket_jenkins, jenkins_name)
        opts = {'build_start_timeout' => 60}
        code = conn.job.build(jenkins_build_name, job_params || {}, opts)
        raise "Problem generate build for run." unless code != 0
        result = nil
        if result.nil?
		    puts "Job Running... Please wait !"
	    end
	    while result.nil? do
		    result = JSON.parse(conn.job.get_build_details(jenkins_build_name,code).to_json, object_class: OpenStruct).result
        end
        result = JSON.parse(conn.job.get_build_details(jenkins_build_name,code).to_json, object_class: OpenStruct).result
        if result == 'SUCCESS'
        then
            puts '[INFO] JOB RUN WITH SUCCESS.'
        else
            puts '[INFO] JOB RUN WITH PROBLEMS.'
            exit 1
        end
    end
    
    def self.rocketchat_query(msg)
        client = RocketChat::Server.new('http://rocketchat.ionutnechita.ro:3000/')
        session = client.login('sander44', '123456@@')
        session.chat.post_message(channel: "#{@channel}", text: msg, as_user: false)
    end
end
