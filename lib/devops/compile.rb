require 'devops/common'

module Devops


	def self.compile_source(timestamp)
		puts '=========================================='
		puts '[INFO] Configure new kernel for start compilation process.'
		Dir.chdir('config/kernel_config')
		var = %x{ pwd && ls }
		exit_code
		puts var
		@var_kernel = %x{ basename #{@working_dir}/#{timestamp}/linux*/}.delete("\n")
		exit_code
		puts var
		var = %x{ cp -v kernel_config_mid #{@working_dir}/#{timestamp}/#{@var_kernel}/.config }
		exit_code
		puts var
		Dir.chdir("#{@working_dir}/#{timestamp}/#{@var_kernel}")
		var = %x{ yes '' | make oldconfig }
		exit_code
		puts var
		var = %x{ make clean }
		exit_code
		puts var
		puts '[INFO] Compile new kernel.'
		Dir.chdir("#{@working_dir}/#{timestamp}/#{@var_kernel}")
		cmd = 'make -j24 deb-pkg LOCALVERSION=-ionutnechita.ro'
		Open3.popen2e(cmd) do |stdin, stdout_err, wait_thr|
			while line = stdout_err.gets
			puts line
			end
		
			exit_status = wait_thr.value
			unless exit_status.success?
			abort "FAILED !!! #{cmd}"
			end
		end
		puts '=========================================='
	end
end