require 'devops/common'

module Devops


    def self.detect_params_kernel(bucket,version_kernel)
        puts '=========================================='
        puts '[INFO] Detect all parameters for kernel.'
        res = db_query("SELECT r1.iseol, r1.version, r1.moniker, r1.source, r1.pgp, r1.released.isodate, r1.released.timestamp, r1.changelog, r1.diffview FROM `#{bucket}` as r unnest r.releases as r1 where r1.version = '#{version_kernel}' limit 1", bucket)
        exist_entry = strip(res).first
        if exist_entry.nil?
        then
            puts '[INFO] Maybe is wrong. Not exist entry. Rerun Download Release and rerun it.'
            exit 1
        else
            @iseol = strip(res).first['iseol']
            @version = strip(res).first['version']
            @moniker = strip(res).first['moniker']
            @source = strip(res).first['source']
            @pgp = strip(res).first['pgp']
            @isodate_kernel = strip(res).first['isodate']
            @unix_timestamp_kernel = strip(res).first['timestamp']
            @changelog = strip(res).first['changelog']
            @diffview =  strip(res).first['diffview']
            puts "[INFO] New detect kernel: #{@version}"
            puts '[INFO] For more information: '
            puts "[INFO] Iseol: #{@iseol}"
            puts "[INFO] Moniker: #{@moniker}"
            puts "[INFO] Source URL: #{@source}"
            puts "[INFO] PGP: #{@pgp}"
            puts "[INFO] Isodate kernel: #{@isodate_kernel}"
            puts "[INFO] Unix timestamp kernel created: #{@unix_timestamp_kernel}"
            puts "[INFO] Changelog: #{@changelog}" 
            puts "[INFO] Diffview: #{@diffview}"
        end
        puts '=========================================='
    end

    def self.check_reserve_machine(bucket_jenkins,jenkins_name,version_kernel)
        puts '=========================================='
        puts '[INFO] Check if the machine has been reserved.'
        jenkins_build_name = 'RESERVE Machine for Build Kernel'
        job_params = { 'VERSION_KERNEL'   => "#{version_kernel}" }
        jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
        puts '=========================================='
    end

    def self.check_inserted_kernel(bucket_status,version_kernel)
        puts '=========================================='
        puts '[INFO] Check latest version kernel is correct inserted.'
        res = db_query("SELECT timestamp_download_build, name_machine_vm_reserved, status from `#{bucket_status}` where name_kernel='#{version_kernel}'", bucket_status)
        exist_entry = strip(res).first
        if exist_entry.nil?
        then
                puts "[INFO] Maybe is wrong. Not exist entry. Rerun it."
                exit 1
        else
                timestamp_download_build = strip(res).first['timestamp_download_build']
                name_machine_vm_reserved = strip(res).first['name_machine_vm_reserved']
                status_build = strip(res).first['status']
                puts "[INFO] Timestamp Download Latest Version Kernel: #{timestamp_download_build}"
                puts "[INFO] Machine VM reserved: #{name_machine_vm_reserved}"
                puts "[INFO] Check if status is started."
                if status_build == 'start'
                then
                    puts "[INFO] Build is present on started. Check env and rerun it."
                    exit 0
                else
                    puts "[INFO] Start NOW new build."
                end
        end
        if @unix_timestamp_kernel.to_s == timestamp_download_build.to_s
        then
            puts "[OK] Is match."
            puts "[INFO] Update build with start timestamp."
            @timestamp = Time.now.to_i
            res = db_query("UPDATE `#{bucket_status}` SET timestamp_start_build='#{@timestamp}', status='start' where name_kernel='#{version_kernel}'", bucket_status)
        else
            puts "[FAIL] Is not match."
            exit 1
        end
        puts '=========================================='
    end

    def self.download_kernel(bucket_jenkins,jenkins_name,machine_ci,source,pgp,timestamp)
        puts '=========================================='
        puts '[INFO] Download new latest version kernel'
        jenkins_build_name = 'DOWNLOAD Source for Build Kernel'
        job_params = { 'HOSTNAME_MACHINE_CI'   => "#{machine_ci}", 'URL_KERNEL'   => "#{source}",  'URL_KERNEL_SIGN'   => "#{pgp}", 'TIMESTAMP' => "#{timestamp}" }
        jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
        puts '=========================================='
    end

    def self.compile_kernel(bucket_jenkins,jenkins_name,machine_ci,timestamp)
        puts '=========================================='
        puts '[INFO] Compile new latest version kernel'
        jenkins_build_name = 'COMPILE Source for Build Kernel'
        job_params = { 'HOSTNAME_MACHINE_CI'   => "#{machine_ci}", 'TIMESTAMP' => "#{timestamp}" }
        jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
        puts '=========================================='
    end

    def self.upload_kernel_vm(bucket_jenkins,jenkins_name,machine_ci,timestamp,bucket_status,bucket_vm,version_kernel)
        puts '=========================================='
        puts '[INFO] Load the kernel version resulting from the compilation'
        jenkins_build_name = 'UPLOAD Image for Build Kernel'
        job_params = { 'HOSTNAME_MACHINE_CI'   => "#{machine_ci}", 'TIMESTAMP' => "#{timestamp}", 'BUCKET_STATUS' => "#{bucket_status}", 'BUCKET_VM' => "#{bucket_vm}", 'VERSION_KERNEL' => "#{version_kernel}" }
        jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
        puts '=========================================='
    end

    def self.build(bucket,bucket_jenkins,bucket_status,bucket_vm,jenkins_name,machine_ci,version_kernel)
        detect_params_kernel(bucket,version_kernel)
        check_reserve_machine(bucket_jenkins,jenkins_name,version_kernel)
        check_inserted_kernel(bucket_status,version_kernel)
        download_kernel(bucket_jenkins,jenkins_name,machine_ci,@source,@pgp,@timestamp)
        compile_kernel(bucket_jenkins,jenkins_name,machine_ci,@timestamp)
        upload_kernel_vm(bucket_jenkins,jenkins_name,machine_ci,@timestamp,bucket_status,bucket_vm,version_kernel)
    end

end