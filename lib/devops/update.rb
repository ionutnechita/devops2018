require 'devops/common'


module Devops


    def self.search_username_and_password(bucket_host,machine_host)
        puts '=========================================='
        puts '[INFO] Search username and password on baremetal server.'

        res = db_query("SELECT * FROM `#{bucket_host}` where meta().id like '%#{machine_host}%'", bucket_host)
        
        @hostname = strip(res).first["#{bucket_host}"]['hostname']
        @ip = strip(res).first["#{bucket_host}"]['ip']
        @password = strip(res).first["#{bucket_host}"]['password']
        @username = strip(res).first["#{bucket_host}"]['username']
        
        if @hostname.nil? or @ip.nil? or @password.nil? or @username.nil?
        then
            puts "[INFO] Problem under detect machine. Check manual variables. Is empty one or more...?"
            puts '=========================================='
            exit 1
        else
            puts "[INFO] Detect this: Hostname is: #{@hostname}, ip is: #{@ip} and username is: #{@username}. Password is secrets. For debug view directly in DB."
        end
        puts '=========================================='
    end

    def self.start_update_upgrade_package(hostname,ip,username,password)
        puts '=========================================='
        puts "[INFO] Start update and upgrade package on baremetal system. #{hostname}"
        cmd = "echo [INFO] Start upgrade distribution linux. && hostname && cat /etc/os-release && dpkg -l > ver1.txt && export DEBIAN_FRONTEND='noninteractive' && apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && dpkg -l > ver2.txt && diff ver1.txt ver2.txt ; echo Number package now: $((`dpkg -l | wc -l`-5)) && rm ver1.txt ver2.txt"
        puts "[INFO] Start cmd: #{cmd}"
        puts var = %x{ export SSHPASS=#{password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{username}@#{ip} '#{cmd}' }
        exit_code
        rocketchat_query(var)
        puts '=========================================='
    end

    def self.update_host(bucket_host,machine_host)
        search_username_and_password(bucket_host,machine_host)
        start_update_upgrade_package(@hostname,@ip,@username,@password)
    end
end


