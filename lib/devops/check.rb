require 'devops/common'


module Devops


    def self.search_machine_and_verify_connectivity_ping(bucket_vm)
        puts '=========================================='
        puts "[INFO] Check echo transport all machine ci."
        res = db_query("SELECT name FROM `#{bucket_vm}`", bucket_vm)
        
        count_machine_ci = strip(res).length
        0.step(count_machine_ci,1) {
            |i|
            if i == count_machine_ci
            then
                    break
            end

            name = strip(val)[i]['name']
            puts "====Ping #{name}===="
            cmd = "ping #{name} -c1"
            puts "[INFO] Start cmd: #{cmd}"
            puts var = `#{cmd}`
            exit_code
        }
        puts "Entry num: #{count_machine_ci}"
        puts '=========================================='
    end
end