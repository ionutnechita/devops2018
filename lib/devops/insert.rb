require 'devops/common'

module Devops

    
    def self.search_username_and_password(bucket_host,machine_host)
        puts '=========================================='
        puts '[INFO] Search username and password on baremetal server.'
        res = db_query("SELECT * FROM `#{bucket_host}` where meta().id like '%#{machine_host}%'", bucket_host)
        @hostname = strip(res).first["#{bucket_host}"]['hostname']
        @ip = strip(res).first["#{bucket_host}"]['ip']
        @password = strip(res).first["#{bucket_host}"]['password']
        @username = strip(res).first["#{bucket_host}"]['username']
        if @hostname.nil? or @ip.nil? or @password.nil? or @username.nil?
        then
            puts "[INFO] Problem under detect machine. Check manual variables. Is empty one or more...?"
            puts '=========================================='
            exit 1
        else
            puts "[INFO] Detect this: Hostname is: #{@hostname}, ip is: #{@ip} and username is: #{@username}. Password is secrets. For debug view directly in DB."
        end
        puts '=========================================='
    end


    def self.list_all_machine_vm(hostname,ip,username,password,group_vm)
        puts '=========================================='
        puts "[INFO] List all machine ci on single variable. Connect on: #{hostname} - (SSH)"
        cmd = "echo $(qm list | grep #{group_vm} | grep -v -)"
        puts "[INFO] Start cmd: #{cmd}"
        puts @all_machine = %x{ export SSHPASS=#{password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{username}@#{ip} '#{cmd}' }
        exit_code
        puts '=========================================='
    end

    def self.parsing_and_insert_db(ip,username,password,bucket_vm,all_machine,user_vm,password_vm)
        puts '=========================================='
        puts "[INFO] Parsing variable all_machine and insert in DB all machine."

        puts "[INFO] Delete all entry in #{bucket_vm}."
        res = db_query("DELETE FROM `#{bucket_vm}`", bucket_vm)

        temp_num1 = 6
        temp_num2 = all_machine.split(' ').length
        temp_num3 = 0
        
        puts "[INFO] Entry to loop for insert multiple documents in DB."
        0.step(temp_num2,6) {
            |i|
            
            if i == temp_num2
            then
                break
            end
            
            vmid = all_machine.split(' ').slice(0+i, temp_num1)[0]
            name = all_machine.split(' ').slice(0+i, temp_num1)[1]
            status = all_machine.split(' ').slice(0+i, temp_num1)[2]
            mem = all_machine.split(' ').slice(0+i, temp_num1)[3]
            disk = all_machine.split(' ').slice(0+i, temp_num1)[4]
            pid = all_machine.split(' ').slice(0+i, temp_num1)[5]

            busy = 'no'

            if status == 'running'
            then
                connectable='yes'
            else
                connectable='no'
            end
            
            cmd = "echo $(qm config #{vmid} | grep net0 | cut -d ':' -f2- | cut -d '=' -f2 | cut -d ',' -f1)"
            puts "[INFO] Start cmd: #{cmd}"
            puts mac_address = %x{ export SSHPASS=#{password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{username}@#{ip} '#{cmd}' }.delete("\n")
            
            puts "[INFO] Insert results in #{bucket_vm}, for use on new build."
            res = db_query("INSERT INTO `#{bucket_vm}` (KEY, VALUE) VALUES (':#{name}', { 'name' : '#{name}', 'status' : '#{status}', 'vmid' : '#{vmid}', 'mem' : '#{mem}', 'disk' : '#{disk}', 'pid' : '#{pid}', 'connectable' : '#{connectable}', 'busy' : '#{busy}', 'user_vm' : '#{user_vm}', 'password_vm' : '#{password_vm}', 'mac_address' : '#{mac_address}', 'ip' : '' })", bucket_vm)
            temp_num3 = temp_num3 + 1
        }
        puts "[INFO] Inserted num: #{temp_num3}"
        puts '=========================================='
    end

    def self.insert_vm(bucket_host,bucket_vm,machine_host,group_vm,user_vm,password_vm)
        search_username_and_password(bucket_host,machine_host)
        list_all_machine_vm(@hostname,@ip,@username,@password,group_vm)
        parsing_and_insert_db(@ip,@username,@password,bucket_vm,@all_machine,user_vm,password_vm)
    end
end