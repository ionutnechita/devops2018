require 'devops/common'
require 'devops/insert'

module Devops


    def self.detect_is_reserved(bucket,bucket_status,version_kernel)
        puts '=========================================='
        puts '[INFO] Check is reserved version kernel.'
        if version_kernel == ''
        then
            puts '[INFO] Please insert correctly version kernel'
            exit 1
        end
        res = db_query("SELECT count(*) from `#{bucket_status}` where meta().id=':#{version_kernel}'", bucket_status)
        number_ifexistkernel = strip(res).first['$1']
        if number_ifexistkernel == 1
        then
            puts '[INFO] Machine VM is reserved. Env is already prepared for start new build.'
            puts '=========================================='
            exit 0
        else
            puts "[INFO] Start new reserve machine ci for #{version_kernel}"
        end
        puts '[INFO] Detect timestamp on version kernel.'
        res = db_query("SELECT r1.released.timestamp FROM `#{bucket}` as r unnest r.releases as r1 where r1.version='#{version_kernel}' limit 1", bucket)
        exist_entry = strip(res).first
        if exist_entry.nil?
        then
            puts '[INFO] Maybe is wrong. Not exist entry. Rerun Download Release and rerun it.'
            exit 1
        end
        @timestamp_download_build = strip(res).first['timestamp']
        if @timestamp_download_build == ''
        then
            puts '[ERROR] Timestamp not detect. Is new version? Maybe verify env and rerun it.'
            puts '=========================================='
            exit 1
        end
        puts "#{@timestamp_download_build}"
        puts '=========================================='
    end

    def self.reserve_machine_vm(bucket_vm,version_kernel)
        puts '=========================================='
        puts '[INFO] Reserve machine VM for start new build.'
        puts '[INFO] Detect reserved machine.'
        res = db_query("select count(*) from `#{bucket_vm}` where busy='#{version_kernel}'", bucket_vm)
        number_ifexistmachine = strip(res).first['$1']
        if number_ifexistmachine != 0
        then
            puts '[INFO] Machine VM is reserved. Env is already prepared for start new build.'
            puts '=========================================='
            return
        else
            puts "[INFO] Start new reserve machine vm for #{version_kernel}"
        end
        puts '[INFO] Detect name machine VM for reserve.'
        res = db_query("SELECT name FROM `#{bucket_vm}` where busy = 'no' order by name asc limit 1", bucket_vm)
        @machine_vm_name = strip(res).first['name']
        if @machine_vm_name == ''
        then
            puts '[ERROR] Is full reserved environment? Maybe verify env and rerun it.'
            puts '=========================================='
            exit 1
        end
        puts "[INFO] Reserve now machine vm #{bucket_vm}."
        res = db_query("UPDATE `#{bucket_vm}` set busy = 'yes,#{version_kernel}' where name = '#{@machine_vm_name}'", bucket_vm)
        puts '=========================================='
    end

    def self.added_reserved_machine(bucket_status,bucket_vm,version_kernel,machine_vm_name,timestamp,ip)
        puts '=========================================='
        puts '[INFO] Added new build on status build bucket container.'
        if machine_vm_name.nil?
        then
            res = db_query("SELECT name FROM `#{bucket_vm}` where busy like '%#{version_kernel}%'", bucket_vm)
            machine_vm_name = strip(res).first['name']
            if machine_vm_name == ''
            then
                puts '[ERROR] Not detected machine.'
                puts '=========================================='
                exit 1
            end
        end
        res = db_query("INSERT INTO `#{bucket_status}` (KEY, VALUE) VALUES (':#{version_kernel}', { 'name_kernel' : '#{version_kernel}', 'name_machine_vm_reserved' : '#{machine_vm_name}', 'status' : 'stop', 'timestamp_download_build' : '#{timestamp}', 'timestamp_start_build' : 'null', 'timestamp_finished_build' : 'null', 'ip' : '#{ip}' })", bucket_status)
        rocketchat_query("[INFO] Reserved env: Version kernel with #{version_kernel} on #{machine_vm_name} with this date: #{timestamp}")
        puts '=========================================='
    end

    def self.check_and_start_vm(bucket_vm,machine_vm_name,bucket_host,machine_host)
        puts '=========================================='
        res = db_query("SELECT status,vmid FROM `#{bucket_vm}` WHERE name='#{machine_vm_name}'", bucket_vm)
        puts status = strip(res).first['status']
        vmid = strip(res).first['vmid']
        if vmid == ''
        then
            puts '[ERROR] Not detected machine.'
            puts '=========================================='
            exit 1
        end
        case status
        when '' 
            puts '[ERROR] Not detected machine.'
            puts '=========================================='
            exit 1
        when 'stopped'
            search_username_and_password(bucket_host,machine_host)
            cmd = "echo $(qm start #{vmid})"
            puts "[INFO] Start cmd: #{cmd}"
            puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
            exit_code
        when 'running'
            search_username_and_password(bucket_host,machine_host)
            cmd = "echo $(qm stop #{vmid})"
            puts "[INFO] Start cmd: #{cmd}"
            puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
            exit_code
            cmd = "echo $(qm start #{vmid})"
            puts "[INFO] Start cmd: #{cmd}"
            puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
            exit_code
        end
        status = 'running'
        puts status
        res = db_query("UPDATE `#{bucket_vm}` set status = '#{status}' where name = '#{machine_vm_name}'", bucket_vm)
        puts '=========================================='
    end

    def self.is_connectable_vm(bucket_vm,machine_vm_name,bucket_host,machine_host)
        puts '=========================================='
        res = db_query("SELECT connectable,mac_address FROM `#{bucket_vm}` WHERE name='#{machine_vm_name}'", bucket_vm)
        connectable = strip(res).first['connectable']
        mac_address = strip(res).first['mac_address']
        if connectable == '' or mac_address == ''
        then
            puts '[ERROR] Not detected machine.'
            puts '=========================================='
            exit 1
        elseif connectable == 'no'
            search_username_and_password(bucket_host,machine_host)
            sleep(60)
            cmd = "echo $(arp-scan -l | grep `echo #{mac_address} |  tr '[:upper:]' '[:lower:]'` | head -n1 | wc -l 2>&1)"
            puts "[INFO] Start cmd: #{cmd}"
            puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
            exit_code
            if res == '1'
            then
                cmd = "echo $(arp-scan -l | grep `echo #{mac_address} |  tr '[:upper:]' '[:lower:]'` | head -n1 | cut -c1-13 2>&1)"
                puts "[INFO] Start cmd: #{cmd}"
                puts @ip1 = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }.delete("\n")
                exit_code         
                cmd = "echo $(ping -c1 #{@ip1})"
                puts "[INFO] Start cmd: #{cmd}"
                puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
                exit_code
            end
        else connectable == 'yes'
            sleep(60)
            cmd = "echo $(arp-scan -l | grep `echo #{mac_address} |  tr '[:upper:]' '[:lower:]'` | head -n1 | cut -c1-13 2>&1)"
            puts "[INFO] Start cmd: #{cmd}"
            puts @ip1 = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }.delete("\n")
            exit_code         
            cmd = "echo $(ping -c1 #{@ip1})"
            puts "[INFO] Start cmd: #{cmd}"
            puts res = %x{ export SSHPASS=#{@password} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{@username}@#{@ip} '#{cmd}' }
            exit_code
        end
        connectable = 'yes'
        puts connectable
        res = db_query("UPDATE `#{bucket_vm}` set connectable = '#{connectable}' where name = '#{machine_vm_name}'", bucket_vm)
        res = db_query("UPDATE `#{bucket_vm}` set ip = '#{@ip1}' where name = '#{machine_vm_name}'", bucket_vm)
        puts '=========================================='
    end

    def self.reserve(bucket,bucket_status,bucket_vm,version_kernel,bucket_host,machine_host)
        detect_is_reserved(bucket,bucket_status,version_kernel)
        reserve_machine_vm(bucket_vm,version_kernel)
        check_and_start_vm(bucket_vm,@machine_vm_name,bucket_host,machine_host)
        is_connectable_vm(bucket_vm,@machine_vm_name,bucket_host,machine_host)
        added_reserved_machine(bucket_status,bucket_vm,version_kernel,@machine_vm_name,@timestamp_download_build,@ip1)
    end
end