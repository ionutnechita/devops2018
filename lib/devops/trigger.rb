require 'devops/common'

module Devops


    def self.trigger_kernel(bucket,bucket_jenkins,jenkins_name,moniker)
        puts '=========================================='
        puts '[INFO] Detect new release.'
        array_version_new = Array.new
        array_version_old = Array.new
        case moniker
        when "linux-next"
            jenkins_build_name = "BUILD Linux-next"
        when "mainline"
            jenkins_build_name = "BUILD Mainline"
        when "stable"
            jenkins_build_name = "BUILD Stable"
        when "longterm"
            jenkins_build_name = "BUILD Longterm"
        else
            puts '[INFO] Inserted moniker is not supported. Tries to enter it correctly.'
            exit 1
        end
        res = db_query("SELECT count(meta(r).id) from `#{bucket}` as r", bucket)
        counter_entry = strip(res).first['$1']
        case counter_entry
        when 0
            exit 1
        when 1
            res = db_query("SELECT meta(r).id from `#{bucket}` as r order by meta(r).id desc limit 1", bucket)
            now_day = strip(res).first['id']
            res = db_query("SELECT count(r1.version) from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{now_day}'", bucket)
            counter_version_now_day = strip(res).first['$1']
            res = db_query("SELECT r1.version from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{now_day}'", bucket)
            0.step(counter_version_now_day ,1) {
                |i|
                if i == counter_version_now_day 
                then
                    break
                end
    
                name = strip(res)[i]['version']
                array_version_new.push(name)
            }
            puts '[INFO] Detect new version kernel'
            puts '[INFO] Difference found this day.'
            version_kernel = array_version_new.join(' ')
            puts version_kernel
            job_params = { 'VERSION_KERNEL'   => "#{version_kernel}" }
            jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
            puts "List all version kernel detect: #{array_version_new}"
        else
            res = db_query("SELECT meta(r).id from `#{bucket}` as r order by meta(r).id desc limit 2", bucket)
            now_day = strip(res).first['id'] 
            old_day = strip(res).last['id']
            res = db_query("SELECT count(r1.version) from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{now_day}'", bucket)
            counter_version_now_day = strip(res).first['$1']
            res = db_query("SELECT count(r1.version) from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{old_day}'", bucket)
            counter_version_old_day = strip(res).first['$1']
            res = db_query("SELECT r1.version from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{now_day}'", bucket)
            0.step(counter_version_now_day,1) {
                |i|
                if i == counter_version_now_day
                then
                    break
                end
    
                name = strip(res)[i]['version']
                array_version_new.push(name)
            }
            res = db_query("SELECT r1.version from `#{bucket}` as r unnest r.releases as r1 where r1.moniker='#{moniker}' and meta(r).id = '#{old_day}'", bucket)
            0.step(counter_version_old_day,1) {
                |i|
                if i == counter_version_old_day
                then
                    break
                end
    
                name = strip(res)[i]['version']
                array_version_old.push(name)
            }
            puts '[INFO] Detect new version kernel'
            if  array_version_new.sort != array_version_old.sort
            then
                puts '[INFO] Difference found this day.'
                version_kernel = (array_version_new.sort - array_version_old.sort).join(' ')
                puts version_kernel

                job_params = { 'VERSION_KERNEL'   => "#{version_kernel}" }
                jenkins_query(jenkins_build_name, job_params, bucket_jenkins, jenkins_name)
            else
                puts '[INFO] No difference found this day.'
            end
            puts "List all version kernel detect: #{array_version_old}, #{array_version_new}"
        end
    end
end