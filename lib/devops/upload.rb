require 'devops/common'


module Devops


    def self.upload_image_vm(timestamp,bucket,bucket_vm,version_kernel)
        puts timestamp
        puts bucket
        puts version_kernel
        Dir.chdir("#{@working_dir}/#{timestamp}")
        var = %x{ ls  }
		exit_code
        puts var
        res = db_query("SELECT name_machine_vm_reserved, ip from `#{bucket}` where name_kernel='#{version_kernel}'", bucket)
        name_machine_vm_reserved = strip(res).first['name_machine_vm_reserved']
        puts ip = strip(res).first['ip']
        res = db_query("SELECT user_vm,password_vm from `#{bucket_vm}` where name='#{name_machine_vm_reserved}'", bucket_vm)
        puts user_vm = strip(res).first['user_vm']
        password_vm = strip(res).first['password_vm']
        cmd = "rm -Rf ~/linux-image*"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{user_vm}@#{ip} '#{cmd}' }
        cmd = "`find . -name 'linux-image*.deb'`"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e scp -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{cmd} '#{user_vm}@#{ip}':~ }
        cmd = "cd /tmp; sudo DEBIAN_FRONTEND=noninteractive apt-get purge linux-image* -y && sudo apt-get autoremove -y"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{user_vm}@#{ip} '#{cmd}' }
        cmd = "cd ~; sudo DEBIAN_FRONTEND=noninteractive apt install ./linux-image*"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{user_vm}@#{ip} '#{cmd}' }
        cmd = "sudo reboot"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{user_vm}@#{ip} '#{cmd}' }
        sleep(30)
        cmd = "uname -a | grep `echo #{version_kernel} | cut -d '-' -f1`  | wc -l"
        puts var = %x{ export SSHPASS=#{password_vm} && sshpass -e ssh -oConnectTimeout=1000 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oServerAliveInterval=30 #{user_vm}@#{ip} '#{cmd}' }.delete("\n")
        if var == '1'
        then
            puts "SUCCESS"
            res = db_query("SELECT * from `machine_host` where meta().id like ':%nexus%'", 'machine_host')
            hostname_artifactory = strip(res).first['machine_host']['hostname']
            password_artifactory = strip(res).first['machine_host']['password']
            username_artifactory = strip(res).first['machine_host']['username']
            puts var = %x{ curl -I http://'#{hostname_artifactory}':8081 }
            cmd = " curl -v -u '#{username_artifactory}':'#{password_artifactory}' --upload-file linux-image*.deb http://'#{hostname_artifactory}':8081/repository/devops/"
            puts var = %x{ #{cmd} }
            exit 0
        else
            puts "FAILED"
            exit 1
        end
    end
end