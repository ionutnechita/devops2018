require 'devops/common'

module Devops


    def self.import_release(bucket)
        puts '=========================================='
        puts '[INFO] Import new release.'
        puts '[INFO] Create workspace for save multiple file release.'
        var = %x{ mkdir -p #{@dir_workspace} }
        exit_code
        puts '[INFO] Change workspace.'
        Dir.chdir("#{@dir_workspace}")
        puts Dir.pwd
        puts '[INFO] Download new file release.'
        var = %x{ wget #{@path_url_kernel_release_json} -O #{@dir_workspace}/#{@timestamp} }
        exit_code
        var = %x{ ls }
        puts '[INFO] Replace characters on json file release for correct syntax import on couchbase server.'
        puts '[INFO] Replace: ' + @ch_1
        var = %x{ sed -i #{@ch_1} #{@dir_workspace}/#{@timestamp}  }
        exit_code
        puts '[INFO] Replace: ' + @ch_2
        var = %x{ sed -i #{@ch_2} #{@dir_workspace}/#{@timestamp}  }
        exit_code
        puts '[INFO] Import file release in couchbase server.'
        puts var = %x{ #{@path_cbimport_couchbase} json -u #{config.user} -p #{config.password} -c couchbase://#{config.cluster} -b #{bucket} -f #{@format} -d file://#{@dir_workspace}/#{@timestamp} -g :#{@timestamp} }
        exit_code
        puts '=========================================='
    end

    def self.download_source(source,pgp,timestamp)
        puts '=========================================='
        puts '[INFO] Download now new source'
        var = %x{ mkdir #{@working_dir}/#{timestamp} }
        puts '[INFO] Change dir for download new source.'
        Dir.chdir("#{@working_dir}/#{timestamp}")
        puts '[INFO] Remove old files.'
        var = %x{ rm -Rf ./* }
        exit_code
        puts '[INFO] Download source in progress.'
        puts " test #{source}" 
        if source == ''
        then
            var = %x{ git clone --depth=1 git://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git }
            return
        end
        var = %x{ wget #{source} }
        exit_code
        if pgp != ''
        then
            puts '[INFO] Download sign key in progress.'
            var = %x{ wget #{pgp} }
            exit_code
            puts '[INFO] Unzip now archive in .tar format'
            var = %x{ unxz *.xz }
            exit_code
            puts '[INFO] Detect signature key.'
            var = %x{ gpg --verify *.sign 2>&1 | grep 'key ID' | awk '{print $NF}' }
            exit_code
            if var == ''
            then
                puts '[INFO] Problem detect signature key.'
                exit 1
            else
                puts '[INFO] The correct signature key found.'
                puts var
            end
            puts '[INFO] Receive official key.'
            var1 = %x{ gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys #{var} }
            var1 = %x{ gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys #{var} }
            exit_code
            puts '[INFO] Check the integrity of the archive.'
            var = %x{ gpg --verify *.sign 2>&1  | grep 'Good signature' }
            exit_code
            puts '[INFO] Unzip now archive'
            var = %x{ tar xvf *.tar }
            exit_code
        else
            puts '[INFO] Unzip now archive'
            var = %x{ tar xvf *.tar.gz }
            exit_code 
        end
        puts '=========================================='
    end
end
