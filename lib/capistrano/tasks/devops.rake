namespace :devops do
  
  desc "Description of task"
  task :hello_world_task do
  puts "Hello World Devops Kernel CI"
  end

  desc "Update Package"
  task :update_package do
    run_locally do
      execute "export LANG=en_US.UTF-8 && export DEBIAN_FRONTEND=noninteractive && apt-get update -y && apt-get upgrade -y && apt-get dist-upgrade -y"
    end
  end
end
