require 'devops/common'
require 'devops/build'
require 'devops/check'
require 'devops/compile'
require 'devops/download'
require 'devops/insert'
require 'devops/reserve'
require 'devops/update'
require 'devops/trigger'
require 'devops/upload'
STDOUT.sync = true

