# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "devops"
set :repo_url, "https://gitlab.com/ionutnechita/devops2018.git"

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/opt/#{fetch(:application)}"

# Default value for :format is :airbrussh.
set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for keep_releases is 5
set :keep_releases, 2

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

# Configs to get `whenever` working
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
set :whenever_roles, :ci